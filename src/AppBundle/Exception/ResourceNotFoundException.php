<?php

namespace AppBundle\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;

class ResourceNotFoundException extends HttpException
{
    public function __construct($message)
    {
        parent::__construct(404, json_encode(['message' => $message]));
    }
}