<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DependantControllerTest extends WebTestCase
{
    public function testGetDependantsAction()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/dependants');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
