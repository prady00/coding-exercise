<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Dependant;
use AppBundle\Entity\Dependent;
use AppBundle\Exception\ResourceNotFoundException;
use AppBundle\Exception\ValidationException;
use FOS\RestBundle\Controller\ControllerTrait;
use Knp\Component\Pager\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use FOS\RestBundle\Controller\Annotations as Rest;

class DependantController extends AbstractController
{
    use ControllerTrait;


    public function __construct()
    {

    }

    /**
     * @Rest\View()
     */
    public function getDependantsAction(Request $request)
    {

        $dependant = $this->getDoctrine()->getRepository('AppBundle:Dependant')->findAll();

        $paginator  = new Paginator();
        $pagination = $paginator->paginate(
            $dependant,
            $request->query->get('page', 1),
            $request->query->get('per_page', 20)
        );

        return $pagination;
    }

    /**
     * @Rest\View()
     */
    public function getDependantAction(?Dependant $dependant)
    {
        if(null == $dependant){
            throw new ResourceNotFoundException('dependant not found');
        }
        return $dependant;
    }

    /**
     * @Rest\View(statusCode=201)
     * @ParamConverter("dependant", converter="fos_rest.request_body")
     * @Rest\NoRoute()
     * @param Dependant|null $dependant
     * @param ConstraintViolationListInterface $validationErrors
     * @return Dependant|null
     */
    public function postDependantsAction(Request $request, ?Dependant $dependant, ConstraintViolationListInterface $validationErrors)
    {
        if(count($validationErrors) > 0){
            throw new ValidationException($validationErrors);
        }

        $employee = $this->getDoctrine()->getRepository('AppBundle:Employee')->find($request->get('employee_id'));

        if(null == $employee){
            throw new ResourceNotFoundException('employee not found');
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($dependant);
        $em->flush();
        return $dependant;
    }

    /**
     * @Rest\View(statusCode=204)
     */
    public function deleteDependantsAction(?Dependant $dependant)
    {
        if(null == $dependant){
            throw new ResourceNotFoundException('dependant not found');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($dependant);
        $em->flush();
        return $dependant;
    }

    /**
     * @Rest\View()
     * @ParamConverter("dependant", converter="fos_rest.request_body")
     * @Rest\NoRoute()
     */
    public function putDependantsAction(Request $request, ?Dependant $dependant, ConstraintViolationListInterface $validationErrors)
    {

        if(null == $dependant){
            throw new ResourceNotFoundException('dependant not found');
        }

        $dependantOld = $this->getDoctrine()->getRepository('AppBundle:Dependant')->find($request->get('id'));

        if(null == $dependantOld){
            throw new ResourceNotFoundException('dependant not found');
        }

        $employeeOld = $this->getDoctrine()->getRepository('AppBundle:Employee')->find($request->get('employee_id'));

        if(null == $employeeOld){
            throw new ResourceNotFoundException('employee not found');
        }

        $em = $this->getDoctrine()->getManager();
        $em->merge($dependant);
        $em->flush();
        return $dependant;
    }

}