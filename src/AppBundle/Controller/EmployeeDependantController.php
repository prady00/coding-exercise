<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Company;
use AppBundle\Exception\ValidationException;
use FOS\RestBundle\Controller\ControllerTrait;
use Knp\Component\Pager\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use FOS\RestBundle\Controller\Annotations as Rest;

class EmployeeDependantController extends AbstractController
{
    use ControllerTrait;


    public function __construct()
    {

    }

    /**
     * @Rest\View()
     */
    public function getEmployeesDependantAction($id, Request $request)
    {

        $employee = $this->getDoctrine()->getRepository('AppBundle:Employee')->find($id);

        if(null == $employee){
            return $this->view(null,404);
        }

        $dependant = $this->getDoctrine()->getRepository('AppBundle:Dependant')->findBy(['employee_id' => $id]);

        $paginator  = new Paginator();
        $pagination = $paginator->paginate(
            $dependant,
            $request->query->get('page', 1),
            $request->query->get('per_page', 20)
        );

        return $pagination;
    }


}