<?php

namespace Tests\AppBundle\Entity;
use AppBundle\Entity\Dependant;
use PHPUnit\Framework\TestCase;

class DependantTest extends TestCase {

    public function testGetName() {
        $entity = new Dependant();
        $entity->setName('pradeep');
        $this->assertEquals('pradeep', $entity->getName());
    }
}