# Test REST APIs

This is a document for the developed REST APIs.

## apiary documentation

https://bayzat2.docs.apiary.io/


## Installing

These instructions assume that you have PHP 7, Apache, MySQL on your local system.

### Checkout the code

You can checkout the code at any place in your system. 

```
git clone https://gitlab.com/prady00/coding-exercise.git PROJECT_FOLDER
```

There is no need to do composer update or install as the vendor's dependencies are being added to git.

### Create a virtual host

Create a virtual host in the Apache web server preferably `symfony.test` and make a entry in host file.
The document root of virtual host should point to web directory of the code inside PROJECT_FOLDER. 


### Configure database and initialize it

Create a blank database `test` and change configuration in `app\config\parameters.yml`

Launch a terminal in your code's directory and execute following command to create all the database tables

```
php bin/console doctrine:schema:update --force
```

### Test the installtion with your browser

point your browser to `symfony.local` you should see a blank array

## Testing

Code includes some phpunit test cases.

### Running test cases

Go to the directory having phpunit.xml and run following command on terminal 

```
php unit
```

You should see something like 

```
PHPUnit 7.1.5 by Sebastian Bergmann and contributors.

......                                                              6 / 6 (100%)

Time: 7.02 seconds, Memory: 20.00MB

OK (6 tests, 6 assertions)

```


## Stack Used

* Symfony Framework v 3.3
* PHP 7
* MySQL 


