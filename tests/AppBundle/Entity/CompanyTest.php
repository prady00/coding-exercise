<?php

namespace Tests\AppBundle\Entity;
use AppBundle\Entity\Company;
use PHPUnit\Framework\TestCase;

class CompanyTest extends TestCase {

    public function testGetName() {
        $company = new Company();
        $company->setName('pradeep');
        $this->assertEquals('pradeep', $company->getName());
    }
}