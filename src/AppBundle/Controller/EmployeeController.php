<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Employee;
use AppBundle\Exception\ResourceNotFoundException;
use AppBundle\Exception\ValidationException;
use FOS\RestBundle\Controller\ControllerTrait;
use Knp\Component\Pager\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use FOS\RestBundle\Controller\Annotations as Rest;

class EmployeeController extends AbstractController
{
    use ControllerTrait;


    public function __construct()
    {

    }

    /**
     * @Rest\View()
     */
    public function getEmployeesAction(Request $request)
    {
        $employees = $this->getDoctrine()->getRepository('AppBundle:Employee')->findAll();

        $paginator  = new Paginator();
        $pagination = $paginator->paginate(
            $employees,
            $request->query->get('page', 1),
            $request->query->get('per_page', 20)
        );

        return $pagination;
    }

    /**
     * @Rest\View()
     */
    public function getEmployeeAction(?Employee $employee)
    {
        if(null == $employee){
            throw new ResourceNotFoundException('employee not found');
        }
        return $employee;
    }

    /**
     * @Rest\View(statusCode=201)
     * @ParamConverter("employee", converter="fos_rest.request_body")
     * @Rest\NoRoute()
     * @param Employee|null $employee
     * @param ConstraintViolationListInterface $validationErrors
     * @return Employee|null
     */
    public function postEmployeesAction(?Employee $employee, ConstraintViolationListInterface $validationErrors, Request $request)
    {
        if(count($validationErrors) > 0){
            throw new ValidationException($validationErrors);
        }

        $company = $this->getDoctrine()->getRepository('AppBundle:Company')->find($request->get('company_id'));

        if(null == $company){
            throw new ResourceNotFoundException('company not found');
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($employee);
        $em->flush();
        return $employee;
    }

    /**
     * @Rest\View(statusCode=204)
     */
    public function deleteEmployeesAction(?Employee $employee)
    {
        if(null == $employee){
            throw new ResourceNotFoundException('employee not found');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($employee);
        $em->flush();
        return $employee;
    }

    /**
     * @Rest\View()
     * @ParamConverter("employee", converter="fos_rest.request_body")
     * @Rest\NoRoute()
     */
    public function putEmployeesAction(Request $request, ?Employee $employee, ConstraintViolationListInterface $validationErrors)
    {
        if(count($validationErrors) > 0){
            if($validationErrors->get(0)->getPropertyPath() != 'email'){
                throw new ValidationException($validationErrors);
            }
        }

        $employeeOld = $this->getDoctrine()->getRepository('AppBundle:Employee')->find($request->get('id'));

        if(null == $employeeOld){
            throw new ResourceNotFoundException('employee not found');
        }

        $companyOld = $this->getDoctrine()->getRepository('AppBundle:Company')->find($request->get('company_id'));

        if(null == $companyOld){
            throw new ResourceNotFoundException('company not found');
        }

        $em = $this->getDoctrine()->getManager();
        $em->merge($employee);
        $em->flush();
        return $employee;
    }

}