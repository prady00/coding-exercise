<?php

namespace Tests\AppBundle\Entity;
use AppBundle\Entity\Employee;
use PHPUnit\Framework\TestCase;

class EmployeeTest extends TestCase {

    public function testGetName() {
        $entity = new Employee();
        $entity->setName('pradeep');
        $this->assertEquals('pradeep', $entity->getName());
    }
}