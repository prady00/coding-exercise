<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CompanyControllerTest extends WebTestCase
{
    public function testGetCompaniesAction()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/companies');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
