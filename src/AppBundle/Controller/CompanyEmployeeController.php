<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Company;
use AppBundle\Exception\ResourceNotFoundException;
use AppBundle\Exception\ValidationException;
use FOS\RestBundle\Controller\ControllerTrait;
use Knp\Component\Pager\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use FOS\RestBundle\Controller\Annotations as Rest;

class CompanyEmployeeController extends AbstractController
{
    use ControllerTrait;


    public function __construct()
    {

    }

    /**
     * @Rest\View()
     */
    public function getCompaniesEmployeesAction($id, Request $request)
    {

        $company = $this->getDoctrine()->getRepository('AppBundle:Company')->find($id);

        if(null == $company){
            throw new ResourceNotFoundException('company not found');
        }

        $employees = $this->getDoctrine()->getRepository('AppBundle:Employee')->findBy(['company_id' => $id]);

        $paginator  = new Paginator();
        $pagination = $paginator->paginate(
            $employees,
            $request->query->get('page', 1),
            $request->query->get('per_page', 20)
        );

        return $pagination;
    }


}