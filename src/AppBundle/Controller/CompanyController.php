<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Company;
use AppBundle\Exception\ResourceNotFoundException;
use AppBundle\Exception\ValidationException;
use FOS\RestBundle\Controller\ControllerTrait;
use Knp\Component\Pager\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use FOS\RestBundle\Controller\Annotations as Rest;

class CompanyController extends AbstractController
{
    use ControllerTrait;


    public function __construct()
    {

    }

    /**
     * @Rest\View()
     */
    public function getCompaniesAction(Request $request)
    {

        $companies = $this->getDoctrine()->getRepository('AppBundle:Company')->findAll();

        $paginator  = new Paginator();
        $pagination = $paginator->paginate(
            $companies,
            $request->query->get('page', 1),
            $request->query->get('per_page', 20)
        );

        return $pagination;
    }

    /**
     * @Rest\View()
     */
    public function getCompanyAction(?Company $company)
    {
        if(null == $company){
            throw new ResourceNotFoundException('company not found');
        }
        return $company;
    }

    /**
     * @Rest\View(statusCode=201)
     * @ParamConverter("company", converter="fos_rest.request_body" )
     * @Rest\NoRoute()
     * @param Company|null $company
     * @param ConstraintViolationListInterface $validationErrors
     * @return Company|null
     */
    public function postCompaniesAction(?Company $company, ConstraintViolationListInterface $validationErrors)
    {

        if(count($validationErrors) > 0){
            throw new ValidationException($validationErrors);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($company);
        $em->flush();
        return $company;
    }

    /**
     * @Rest\View(statusCode=204)
     */
    public function deleteCompaniesAction(?Company $company)
    {
        if(null == $company){
            throw new ResourceNotFoundException('company not found');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($company);
        $em->flush();
        return $company;
    }

    /**
     * @Rest\View()
     * @ParamConverter("company", converter="fos_rest.request_body")
     * @Rest\NoRoute()
     * @return Company|\FOS\RestBundle\View\View|null
     */
    public function putCompaniesAction(Request $request, ?Company $company, ConstraintViolationListInterface $validationErrors)
    {
        if(count($validationErrors) > 0){
            throw new ValidationException($validationErrors);
        }

        $companyOld = $this->getDoctrine()->getRepository('AppBundle:Company')->find($request->get('id'));

        if(null == $companyOld){
            throw new ResourceNotFoundException('company not found');
        }

        $em = $this->getDoctrine()->getManager();
        $em->merge($company);
        $em->flush();
        return $company;
    }

}